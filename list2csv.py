import urllib.request

import yaml
from paramiko.client import AutoAddPolicy, SSHClient
from pythonping import ping


def get_machine_status(inventory_url, filename):
    get_url = urllib.request.urlopen(inventory_url)

    dict = yaml.safe_load(get_url.read().decode("utf-8"))["all"]["hosts"]

    client = SSHClient()

    csv_file = open(filename, "a")

    csv_file.write("machine,machine_status,ioc,ioc_status,source\n")

    for key, value in dict.items():

        response_ping = ""
        try:
            response_ping = ping(key, timeout=1, count=1)
        except RuntimeError:
            pass

        host_format = "{},{},{},{},{}\n"

        if response_ping == "":
            machine_status = "unknown hostname"
        elif response_ping.stats_packets_returned == 0:
            machine_status = "offline"
        elif response_ping.stats_packets_returned == 1:
            machine_status = "online"

            # print("List of IOCs from MACHINE")
            client.set_missing_host_key_policy(AutoAddPolicy)
            client.connect(key, username="iocuser", password="vaha23neca")
            ssh_response = client.exec_command("console -u")
            console_output = ssh_response[1].read().decode("utf-8")

            parsed_console_output = console_output.split()

            for ioc_name_idx in range(0, len(parsed_console_output), 3):
                ioc_name = parsed_console_output[ioc_name_idx]
                ioc_status = parsed_console_output[ioc_name_idx + 1]

                csv_file.write(
                    host_format.format(
                        key, machine_status, ioc_name, ioc_status, "machine"
                    )
                )

            client.close()

        for ioc in value["iocs"]:
            csv_file.write(
                host_format.format(
                    key, machine_status, ioc["name"], "unknown", "inventory"
                )
            )

    csv_file.close()


def main():

    gl_group = "https://gitlab.esss.lu.se/nice/inventories/"

    file_path = "/-/raw/master/inventory.yaml"

    inventories = ["ecdc-dev", "nss-sampleenv", "nss-embla", "nss-utgard"]

    for inventory in inventories:
        get_machine_status(gl_group + inventory + file_path, "output.csv")


if __name__ == "__main__":
    main()

# Inventory list

## Dependencies

```
pip install pythonping pyyaml paramiko urllib3
```

## Usage

### List on screen

Add your the inventories that you want to check on `inventories` variable on `main()` function of `list.py` file.

Run the code using `python list.py`.

### List to file

Add your the inventories that you want to check on `inventories` variable on `main()` function of `list2csv.py` file.

Run the code using `python list2csv.py`. It will write the `output.csv` on the same directory as `list2csv.py`.

## Output description

Get the status of the machines on the inventories files. It will test the machine connection using ping:

| Machine status | Description |
|-|-|
| `UNKNOWN HOSTNAME` | The host does not exist or it was modified|
| `ONLINE`           | The machine responds to ping          |
| `OFFLINE`          | The machine does not responds to ping |

The program will query the IOCs status of each `ONLINE` machine using `console -u` and show the response on the screen, they will appear under `List of IOCs from MACHINE` message .

The IOCs of all machines will be shown quering the inventory info, they will appear under `List of IOCs from INVENTORY` message.

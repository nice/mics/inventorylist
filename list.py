import urllib.request

import yaml
from paramiko.client import AutoAddPolicy, SSHClient
from pythonping import ping


def get_machine_status(inventory_url):
    get_url = urllib.request.urlopen(inventory_url)

    print("\n######################################")
    print("Reading inventory: {}".format(inventory_url))

    dict = yaml.safe_load(get_url.read().decode("utf-8"))["all"]["hosts"]

    client = SSHClient()

    for key, value in dict.items():

        response_ping = ""
        try:
            response_ping = ping(key, timeout=1, count=1)
        except RuntimeError:
            pass

        host_format = "\nHOST: {:<40}Status: {:<25}"
        if response_ping == "":
            print(host_format.format(key, "[UNKNOWN HOSTNAME]"))

            print("List of IOCs from INVENTORY")
            for ioc in value["iocs"]:
                print(" {}".format(ioc["name"]))

        elif response_ping.stats_packets_returned == 0:
            print(host_format.format(key, "[OFFLINE]"))

            print("List of IOCs from INVENTORY")
            for ioc in value["iocs"]:
                print(" {}".format(ioc["name"]))

        elif response_ping.stats_packets_returned == 1:
            print(host_format.format(key, "[ONLINE]"))

            print("List of IOCs from MACHINE")
            client.set_missing_host_key_policy(AutoAddPolicy)
            client.connect(key, username="iocuser", password="vaha23neca")
            ssh_response = client.exec_command("console -u")
            print(ssh_response[1].read().decode("utf-8"))
            client.close()

            print("List of IOCs from INVENTORY")
            for ioc in value["iocs"]:
                print(" {}".format(ioc["name"]))


def main():

    gl_group = "https://gitlab.esss.lu.se/nice/inventories/"

    file_path = "/-/raw/master/inventory.yaml"

    inventories = ["ecdc-dev", "nss-sampleenv", "nss-embla", "nss-utgard"]

    for inventory in inventories:
        get_machine_status(gl_group + inventory + file_path)


if __name__ == "__main__":
    main()
